package com.kp.springbootmybatis.mapper;

import com.kp.springbootmybatis.domain.Employee;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    @Select("SELECT * FROM employee")
    List<Employee> getEmployees();

    @Insert("Insert into employee(fullname, username, department) values (#{fullName}, #{userName}, #{department})")
    Integer saveEmployee(Employee employee);

    @Update("Update employee set department= #{department} where id=#{id}")
    public void updateEmployee(Employee employee);

    @Delete("Delete from employee where id=#{id}")
    public void deleteEmployeeById(int id);

    @Select("SELECT * FROM employee WHERE id = #{id}")
    Employee getEmployee(int id);
}
