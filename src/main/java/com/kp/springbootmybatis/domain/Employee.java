package com.kp.springbootmybatis.domain;
import lombok.Data;

@Data
//Sample pojo class
public class Employee {

    private int id;
    private String fullName;
    private String userName;
    private String department;

    public Employee(String fullName, String userName, String department) {
        this.fullName = fullName;
        this.userName = userName;
        this.department = department;
    }
}